import requests

from cfdi_to_xml import Comprobante

# Escenarios para CFDI de Ingreso 4.0


def test_escenario_1():
    """
    Objetivo del escenario:
    1. Validar que en el xml salgan los 3 conceptos/movimientos
    2. Validar que permita usar decimales en los importes
    3. Validar que el subtotal se calcule
    4. Validar que el IVA se calcule de acuerdo al/los impuestos incluidos
    5. Validar que en el xml se desglocen los impuestos
    """
    comprobante = Comprobante(
        receptor=Comprobante.Receptor(
            rfc="EKU9003173C9",
            nombre="ESCUELA KEMPER URGATE",
            domicilio_fiscal_receptor="26015",
            regimen_fiscal_receptor="601",
            uso_cfdi="G03",
        ),
        emisor=Comprobante.Emisor(
            rfc="CACX7605101P8", nombre="XOCHILT CASAS CHAVEZ", regimen_fiscal="612"
        ),
        conceptos=Comprobante.Conceptos(
            [
                Comprobante.Conceptos.Concepto(
                    descuento=0.0,
                    clave_prod_serv="23121615",
                    cantidad=1.00,
                    clave_unidad="H87",
                    descripcion="DESK0004 E-COM07",
                    valor_unitario=1100.00,
                    importe="IMPORTE",
                    objeto_imp="02",
                    impuestos=Comprobante.Conceptos.Concepto.Impuestos(
                        Comprobante.Conceptos.Concepto.Impuestos.Traslados(
                            [
                                Comprobante.Conceptos.Concepto.Impuestos.Traslados.Traslado(
                                    base="BASE",
                                    impuesto="002",
                                    tipo_factor="Tasa",
                                    tasa_ocuota=0.16,
                                    importe="IMPORTE",
                                )
                            ]
                        )
                    ),
                ),
            ]
        ),
        fecha="2022-04-13T11:19:31",
        # TODO Why is Version set by default to "4.0"?
        # version = "4.0",
        forma_pago="99",
        condiciones_de_pago="Plazo de pago: 15 Días",
        moneda="MXN",
        tipo_de_comprobante="I",
        exportacion="01",
        metodo_pago="PPD",
        lugar_expedicion="62000",
        sub_total="SUBTOTAL",
        impuestos=Comprobante.Impuestos(
            total_impuestos_trasladados="TOTAL TRASLADADOS",
            traslados=Comprobante.Impuestos.Traslados(
                [
                    Comprobante.Impuestos.Traslados.Traslado(
                        base="BASE TRASLADO",
                        impuesto="002",
                        tipo_factor="Tasa",
                        tasa_ocuota=0.16,
                        importe="TOTAL TRASLADO",
                    ),
                ]
            ),
        ),
        total="TOTAL",
    )
    comprobante.sign(
        "Gq6t9susmJf1XgOuizZgrBotOqqtPfKHG6xDwogYASBzb7jtx7Ohoi6WF66LVqrA7x7VRPYq1OnJ87+s5bdIy0qdvbHSHJHOjhOc+OgjnqdOmiE65NapPgo6w/wEUakyVWteZjNWTMXUQg2BXiqnDqHLirSInyUdTETDQuq1BHBopDYFIXyE6Vxm2SYVD93o5AyrCHU1UJfZyLvdlOdJKxKPWzWq55mG7EqRVC4rbclfb7Vtc/O/sXtUZscFrAahzGSx/7MKkftJc2aNZ0X4a3LNP4HMZhe9VlBcih/jtUee9mySIL5G8tZkZwTFzRpwJXhMODzSbtJsTehJK3eL3g==",
        "MIIF/TCCA+WgAwIBAgIUMDAwMDEwMDAwMDA1MDM5ODkyMzkwDQYJKoZIhvcNAQELBQAwggGEMSAwHgYDVQQDDBdBVVRPUklEQUQgQ0VSVElGSUNBRE9SQTEuMCwGA1UECgwlU0VSVklDSU8gREUgQURNSU5JU1RSQUNJT04gVFJJQlVUQVJJQTEaMBgGA1UECwwRU0FULUlFUyBBdXRob3JpdHkxKjAoBgkqhkiG9w0BCQEWG2NvbnRhY3RvLnRlY25pY29Ac2F0LmdvYi5teDEmMCQGA1UECQwdQVYuIEhJREFMR08gNzcsIENPTC4gR1VFUlJFUk8xDjAMBgNVBBEMBTA2MzAwMQswCQYDVQQGEwJNWDEZMBcGA1UECAwQQ0lVREFEIERFIE1FWElDTzETMBEGA1UEBwwKQ1VBVUhURU1PQzEVMBMGA1UELRMMU0FUOTcwNzAxTk4zMVwwWgYJKoZIhvcNAQkCE01yZXNwb25zYWJsZTogQURNSU5JU1RSQUNJT04gQ0VOVFJBTCBERSBTRVJWSUNJT1MgVFJJQlVUQVJJT1MgQUwgQ09OVFJJQlVZRU5URTAeFw0yMDA1MTYwMjE2MTlaFw0yNDA1MTYwMjE2MTlaMIHLMSgwJgYDVQQDEx9NT0lTRVMgQUxFSkFORFJPIE5BVkFSUk8gUFJFU0FTMSgwJgYDVQQpEx9NT0lTRVMgQUxFSkFORFJPIE5BVkFSUk8gUFJFU0FTMSgwJgYDVQQKEx9NT0lTRVMgQUxFSkFORFJPIE5BVkFSUk8gUFJFU0FTMRYwFAYDVQQtEw1OQVBNOTYwODA5Nk44MRswGQYDVQQFExJOQVBNOTYwODA5SEpDVlJTMDcxFjAUBgNVBAsTDU9kb29IdW1hbnl0ZWswggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCYiGUCSSKrQQoXhwyNUOJqYicYdlaya4aHcLhFsNEb8OR2lMU2oepw07YKgDbm4ybV3drHBCAdRpsL/FOs7ZBHVt323nsv50MLI5uIP0SHfH2bbp3VXCHdSWSjtJyo840JbMJgdh5vDGVqE+TJ35JFcliPdAkY+k2qQiY02wL3yJJq/VnmjUueXnOmThucsD5xW/V6SenSg3cuyXUnY4AhaC2w6BKn8+xFUY7Oy6KC0XUBSlnOT4xKogTEj7dnyH3MkJsy3A4+9OmvVe1m75bK8dSdw28/fERHHm6DwKFJ1yBRG+Yf2iELN6kBnVUz4Gf1va+y4qn+BRdf1G5YpWxHAgMBAAGjHTAbMAwGA1UdEwEB/wQCMAAwCwYDVR0PBAQDAgbAMA0GCSqGSIb3DQEBCwUAA4ICAQABNRrVSYc+POlgRMNRn5XYzm3zRUUVCPhlw7gMxI2p2fORJr/4rfWRmi2wqRpD/Z3TtdR9Vu5QLlq9omBUxKMJ+bacY3tyDcmyTVuhijT8d/fyn460+JMFBU6jJ3TlRPxMAc+FKG39xpO90mwvHYRcN26XxRy+XulWQflHNHquNINoffTJ3Ty/x2g5rKi1dk2g9aHRUo3kMx1c0QC4pCOQfRdvq0XjIc0tvBgKY/MDIwKRk/YK3lpV9J00DSwbYRQHiVWhYBRLmga73oS7PalUqzxuxvlRoSMvikJgFmZrhhUYcFsXKhNLvxP5hIhpf6FzmjXRE6nBlCtf2W+j9loNEDHDs1rXhqNjaTrykqvypB9/1PZz5eQEp5q6UyC+ozRcsYLt/sZhuT1FRF89qmBN2J+ywzUhRb63lGRUT3D+E5/TvaDgg3bHIJgY1cwbttANFsV4GLsTB3tYGRMiIUhgE2hjNonebZey3vxuSohQ+QClgl+ZJofrwr9FK/0NXiTKkwsaVO2R/APVQk1zUP9lU7q5zNiIOCpUQ0Uj7thh74klp9PVNVFXPSOORANQui9R3HaXzvSpak+SmWKnmXv4YhXGs8gQwS1LxQE49G4sDIK64CnL7yXgpZH/5F3jsv2NCqBZbx5LL/5iZVjL6bjmsIlXbqpi9MYssF5tRjnmOw==",
        "00001000000503989239",
    )

    url_auth = "https://services.test.sw.com.mx/security/authenticate"
    headers = {
        "user": "atencionaclientes@teknosoft.mx",
        "password": "TEKNO+SW",
    }
    response = requests.post(url_auth, headers=headers)
    data = response.json()["data"]
    token = data["token"]
    xml_str = comprobante.to_xml()
    payload = f"""\
------=_Part_11_11939969.1490230712432
Content-Type: text/xml
Content-Transfer-Encoding: binary
Content-Disposition: form-data; name=xml; filename=xml

{xml_str}
------=_Part_11_11939969.1490230712432--"""
    payload = payload.replace("\n", "\r\n")  # Windows line ending
    url = "http://services.test.sw.com.mx/cfdi33/stamp/v4"
    headers = {
        "Authorization": f"bearer {token}",
        "Content-Type": 'multipart/form-data; boundary="----=_Part_11_11939969.1490230712432"',
    }
    result = requests.post(url, headers=headers, data=payload)
    print(payload)
    print(result.status_code)
    print(result.json())
