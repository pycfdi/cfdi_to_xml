def read_file_str(path: str) -> str:
    with open(path, "r", encoding="UTF-8") as file:
        return file.read().strip()
