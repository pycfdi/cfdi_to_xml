create_package_from_xsd:
	git -C src/resources-sat-xml checkout .
	git -C src/resources-sat-xml pull
	cat src/resources-sat-xml/resources/www.sat.gob.mx/sitio_internet/cfd/catalogos/catCFDI.xsd | \
            sed '/<xs:simpleType name="c_CodigoPostal">/,/<\/xs:simpleType>/d' | \
            sed '/<xs:simpleType name="c_ClaveProdServ">/,/<\/xs:simpleType>/d' | \
            sed '/<xs:simpleType name="c_ClaveUnidad">/,/<\/xs:simpleType>/d' | \
            sed '/<xs:simpleType name="c_Colonia">/,/<\/xs:simpleType>/d' | \
            sed '/<xs:simpleType name="c_Localidad">/,/<\/xs:simpleType>/d' | \
            sed '/<xs:simpleType name="c_Municipio">/,/<\/xs:simpleType>/d' \
			> src/resources-sat-xml/resources/www.sat.gob.mx/sitio_internet/cfd/catalogos/catCFDI_resumed.xsd
	mv src/resources-sat-xml/resources/www.sat.gob.mx/sitio_internet/cfd/catalogos/catCFDI.xsd src/resources-sat-xml/resources/www.sat.gob.mx/sitio_internet/cfd/catalogos/catCFDI.xsd~
	mv src/resources-sat-xml/resources/www.sat.gob.mx/sitio_internet/cfd/catalogos/catCFDI_resumed.xsd src/resources-sat-xml/resources/www.sat.gob.mx/sitio_internet/cfd/catalogos/catCFDI.xsd
	xsdata --relative-imports -ss clusters -ds Google src/resources-sat-xml/resources/www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd -p cfdv40
	mv cfdv40 src/cfdi_to_xml/
