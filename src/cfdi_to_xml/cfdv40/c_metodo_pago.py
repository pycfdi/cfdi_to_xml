from enum import Enum

__NAMESPACE__ = "http://www.sat.gob.mx/sitio_internet/cfd/catalogos"


class CMetodoPago(Enum):
    PUE = "PUE"
    PPD = "PPD"
