from enum import Enum

__NAMESPACE__ = "http://www.sat.gob.mx/sitio_internet/cfd/catalogos"


class CRegimenFiscal(Enum):
    VALUE_601 = "601"
    VALUE_603 = "603"
    VALUE_605 = "605"
    VALUE_606 = "606"
    VALUE_607 = "607"
    VALUE_608 = "608"
    VALUE_609 = "609"
    VALUE_610 = "610"
    VALUE_611 = "611"
    VALUE_612 = "612"
    VALUE_614 = "614"
    VALUE_615 = "615"
    VALUE_616 = "616"
    VALUE_620 = "620"
    VALUE_621 = "621"
    VALUE_622 = "622"
    VALUE_623 = "623"
    VALUE_624 = "624"
    VALUE_625 = "625"
    VALUE_626 = "626"
    VALUE_628 = "628"
    VALUE_629 = "629"
    VALUE_630 = "630"
