from enum import Enum

__NAMESPACE__ = "http://www.sat.gob.mx/sitio_internet/cfd/catalogos"


class CObjetoImp(Enum):
    VALUE_01 = "01"
    VALUE_02 = "02"
    VALUE_03 = "03"
