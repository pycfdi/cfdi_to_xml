from enum import Enum

__NAMESPACE__ = "http://www.sat.gob.mx/sitio_internet/cfd/catalogos"


class CImpuesto(Enum):
    VALUE_001 = "001"
    VALUE_002 = "002"
    VALUE_003 = "003"
