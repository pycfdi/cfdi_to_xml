from enum import Enum

__NAMESPACE__ = "http://www.sat.gob.mx/sitio_internet/cfd/catalogos"


class CTipoDeComprobante(Enum):
    I = "I"
    E = "E"
    T = "T"
    N = "N"
    P = "P"
